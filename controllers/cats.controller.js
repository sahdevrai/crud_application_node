const Cat = require("../models/cats.model");

// Create and Save a new cat record
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  // Create a Cat
  const cat = new Cat({
    id: req.body.id,
    name: req.body.name,
    age: req.body.age,
    breed: req.body.breed || false
  });

  // Save Cat in the database
 Cat.create(cat, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Cat."
      });
    else res.send(data);
  });
};

exports.findAll = (req, res) => {
    const title = req.query.title;

   Cat.getAll(title, (err, data) => {
      if (err)
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving cat's data."
        });
      else res.send(data);
    });
  };



// Retrieve all cat records from the database (with condition).
exports.findWithCond = (req, res) => {
  const age = req.query.age;

  Cat.findWithCondition(age,(err,data)=>{
    if (err){
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving cat's data."
        });
    }
      else res.send(data);
 })
};




// Find a single Cat by Id
exports.findOne = (req, res) => {
 Cat.findById(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Cat with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Cat with id " + req.params.id
        });
      }
    } else res.send(data);
  });
};



// Update a Cat identified by the id in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

  console.log(req.body);

 Cat.updateById(
    req.params.id,
    new Cat(req.body),
    (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Cat with id ${req.params.id}.`
          });
        } else {
          res.status(500).send({
            message: "Error updating Cat with id " + req.params.id
          });
        }
      } else res.send(data);
    }
  );
};

// Delete a Cat with the specified id in the request
exports.delete = (req, res) => {
 Cat.remove(req.params.id, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Cat with id ${req.params.id}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete Cat with id " + req.params.id
        });
      }
    } else res.send({ message: ` Cat was deleted successfully!` });
  });
};

// Delete all cat records from the database.
exports.deleteAll = (req, res) => {
 Cat.removeAll((err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all cat's data."
      });
    else res.send({ message: `All cat records were deleted successfully!` });
  });
};