// module.exports = app => {
const express = require('express')
 const router = express.Router()
 const cats =   require('../controllers/cats.controller.js');



router.use((req, res, next) => {
  console.log('Time: ', Date.now())
  next()
})

// router.get('/home', (req, res) => {
//     res.send('Birds home page')
//   })
 router.get('/cats', cats.findAll);
 router.post('/cats', cats.create);
 router.get('/cats/:id', cats.findOne);
 router.get("/cats?age<:cond", cats.findWithCond);
 router.put('/cats/:id', cats.update);
 router.delete('/cats/:id', cats.delete);
 router.delete("/cats", cats.deleteAll);

// // app.use('/api/cats', router);
module.exports = router