var sql = require('../config/db.config');
//Employee object create
var Cat = function(cat){
    this.name = cat.name;
    this.age = cat.age;
    this.breed = cat.breed;
};


Cat.create = (newTutorial, result) => {
    sql.query("INSERT INTO Cats SET ?", newTutorial, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      console.log("created cat: ", { id: res.insertId, ...newTutorial });
      result(null, { id: res.insertId, ...newTutorial });
    });
  };
  
  Cat.findById = (id, result) => {
    sql.query(`SELECT * FROM Cats WHERE id = ${id}`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found cat: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      // not found Cat with the id
      result({ kind: "not_found" }, null);
    });
  };
  
  
  Cat.findWithCondition= (age,result)=>{
    sql.query(`SELECT * FROM Cats where ${age}>2 && ${age}<8`,(err,res)=>{
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found cat: ", res[0]);
        result(null, res[0]);
        return;
      }
  
      // not found Cat with the id
      result({ kind: "not_found" }, null);
    });
  };
  
  Cat.getAll = (name, result) => {
    let query = "SELECT * FROM Cats";
  
    if (name) {
      query += ` WHERE name LIKE '%${name}%'`;
    }
  
    sql.query(query, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log("cat: ", res);
      result(null, res);
    });
  };
  
  
  Cat.updateById = (id, cat, result) => {
    sql.query(
      "UPDATE cats SET name = ?, age = ?, breed = ? WHERE id = ?",
      [cat.name, cat.age, cat.breed, id],
      (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
  
        if (res.affectedRows == 0) {
          // not found Cat with the id
          result({ kind: "not_found" }, null);
          return;
        }
  
        console.log("updated cat: ", { id: id, ...cat });
        result(null, { id: id, ...cat });
      }
    );
  };
  
  Cat.remove = (id, result) => {
    sql.query("DELETE FROM cats WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      if (res.affectedRows == 0) {
        // not found Cat with the id
        result({ kind: "not_found" }, null);
        return;
      }
  
      console.log("deleted cat with id: ", id);
      result(null, res);
    });
  };
  
  Cat.removeAll = result => {
    sql.query("DELETE FROM cats", (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
  
      console.log(`deleted ${res.affectedRows} cat`);
      result(null, res);
    });
  };
  
  module.exports = Cat;